import React, {useContext, useEffect, useState} from 'react';
import {ScrollView, StyleSheet, View, RefreshControl, Alert} from 'react-native';
import {Card, Text, Button, FAB} from 'react-native-elements';
import {LinearGradient} from "expo-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome";
import FormData from "form-data";
import MapView, {Marker, Circle} from "react-native-maps";
import {UserContext} from "./userContext";
import {AlertContext} from "./alertContext";

const ProfilAlert = ({ navigation }) => {
	const [refreshing, setRefreshing] = React.useState(false);

	const {setAlerts, myAlerts, deleteAlerts} = useContext(AlertContext)
	const {userToken} = useContext(UserContext)

	const onRefresh = React.useCallback(async () => {
		setAlerts(userToken);
	}, []);

	useEffect(() => {
		setAlerts(userToken)
	}, [])

	function getAnimalType(type){
		switch (type){
			case "dog":
				return "Chien"
			case "cat":
				return "Chat"
			case "snake":
				return "Serpent"
			case "fish":
				return "Poisson"
			case "bird":
				return "Oiseau"
			case "rat":
				return "Rat"
			default:
				return "N/A"
		}
	}

	async function deleteAlert(alertID){
		setRefreshing(true);

		const delete_form = new FormData();
		delete_form.append('id', alertID)

		await deleteAlerts({
			method: 'delete',
			url: `http://6573-109-9-150-123.ngrok.io/api/search`,
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${userToken}`
			},
			data : delete_form
		})
		await setAlerts(userToken)
		setRefreshing(false)
	}

	return (
		<View style={styles.container}>
			<LinearGradient
				// Background Linear Gradient
				colors={['rgb(109,149,224)', 'transparent']}
				style={styles.background}
			/>
			<ScrollView
				refreshControl={
					<RefreshControl
						refreshing={refreshing}
						onRefresh={onRefresh}
					/>
				}>
				{ myAlerts.length > 0 ?
					myAlerts.map((item, i) => (
						<Card key={i}>
							<Card.Title>{getAnimalType(item.animal)}</Card.Title>
							<Card.Divider/>
							<View style={styles.fieldset}>
								<MapView
									showsUserLocation={false}
									style={styles.map}
									region={{
										latitude: item.location[1],
										longitude: item.location[0],
										latitudeDelta: 0.20,
										longitudeDelta: 0.20,
									}} >
									<Marker coordinate={{'latitude': item.location[1], 'longitude': item.location[0]}}>
									</Marker>
									<Circle
										center = {{
											latitude: item.location[1],
											longitude: item.location[0],
										}}
										radius = { item.range*100 }
										strokeWidth = { 1 }
										strokeColor = { '#1a66ff' }
										fillColor = { 'rgba(20,124,219, 0.4)' }
									>
									</Circle>
								</MapView>
							</View>
							<View>
								{/*<Text style={styles.name}>
									<Icon
										name="map-marker"
										size={15}
										color="black"
									/> {item.location[1]} - {item.location[1]} ({item.range} Km)
								</Text>*/}
								<Text style={styles.name}>
									<Icon
										name="clock-o"
										size={15}
										color="black"
									/> {item.datetime}
								</Text>

							</View>
							<Card.Divider/>
							<Button
								icon={
									<Icon
										name="trash"
										size={15}
										color="red"
									/>
								}
								onPress={() => deleteAlert(item.id)}
								type="outline"
								title=" Supprimer"
							/>
						</Card>
					)): <Card>
							<View>
								<Text>Vous n'avez aucune alerte...</Text>
							</View>
						</Card>
				}
			</ScrollView>
			<FAB title="Ajouter une alerte"
				 onPress={() => navigation.navigate('profil_add_alertes')}
				 icon={
					 <Icon
						 name="plus-circle"
						 size={15}
						 color="white"
					 />
				 } color="green" size="large" style={styles.btn_add_alert}>
			</FAB>
		</View>
	);
}

export default ProfilAlert;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#1c84a3',
		paddingBottom: 20,
	},
	background: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		height: 300,
	},
	btn_add_alert: {
		borderRadius: 30,
		position: 'absolute',
		bottom: 10,
		right: 10,
	},
	map: {
		height: 200
	}
});
