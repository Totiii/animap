import React, {useContext, useEffect, useRef, useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {UserContext} from "./userContext";
import {ListItem, Icon, Avatar} from 'react-native-elements';
import {LinearGradient} from "expo-linear-gradient";
import * as Notifications from "expo-notifications";
import Pusher from "pusher-js/react-native";
import Toast from "react-native-toast-message";

const Profil = ({ navigation }) => {

	const setUpNotifications = async () => {
		const responseListener = useRef();

		responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
			if (response.notification.request.content.data.type === "search_found") {
				navigation.navigate('detail_annonce',
					{"markerElem": response.notification.request.content.data.search_info}
				)
			}
		});

		var pusher = new Pusher('519874f90054bc2bcc8e', {
			cluster: 'eu'
		});
		var channel = await pusher.subscribe(userProfile["username"]);
		channel.bind('search_found', function(data) {
			Toast.show({
				type: 'info',
				text1: 'Un utilisateur à trouvé un animal qui correspond à une de vos recherches.',
				text2: 'Appuyer pour plus de détails.',
				onPress: () => {navigation.navigate('detail_annonce',{"markerElem": data["search_info"]})}
			});
		})
	}

	const {setUserToken, userProfile, setUserInfo} = useContext(UserContext)

	function capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	function logOut(){
		setUserToken('');
		setUserInfo('');
		navigation.navigate('home');
	}

	setUpNotifications()

	const list = [
		{
			title: 'Éditer mes informations',
			icon: 'edit',
			page: 'profil_edit'
		},
		{
			title: 'Mes alertes',
			icon: 'add-alert',
			page: 'profil_alertes'
		},
		{
			title: 'Mes signalements',
			icon: 'campaign',
			page: 'profil_annonces'
		}
	]

	console.log(userProfile['avatar'])

	return (
		<View style={styles.container}>
			<LinearGradient
				// Background Linear Gradient
				colors={['rgb(109,149,224)', 'transparent']}
				style={styles.background}
			/>
			<View style={{ flex: 2, alignItems: 'center', }}>
				<Avatar
					rounded
					size="xlarge"
					source={{
						uri: userProfile['avatar'],
					}}
				/>
				<Text style={{ marginTop:5, color: 'white' }}>{capitalizeFirstLetter(userProfile['first_name'])} {capitalizeFirstLetter(userProfile['last_name'])}</Text>
				<Text style={{ marginTop:5, color: 'white' }}>{userProfile['phone']}</Text>
			</View>

			<View style={{ flex: 3}}>
				{
					list.map((item, i) => (
						<ListItem key={i} onPress={() => navigation.navigate(item.page)} style={styles.button} bottomDivider>
							<Icon name={item.icon} />
							<ListItem.Content>
								<ListItem.Title>{item.title} </ListItem.Title>
							</ListItem.Content>
							<ListItem.Chevron />
						</ListItem>
					))
				}
				<ListItem onPress={logOut} style={styles.button} bottomDivider>
					<Icon name='logout' />
					<ListItem.Content>
						<ListItem.Title>Déconnexion</ListItem.Title>
					</ListItem.Content>
					<ListItem.Chevron />
				</ListItem>
			</View>
		</View>
	);
}

export default Profil;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
		backgroundColor: '#1c84a3',
		justifyContent: 'center',
	},
	avatar: {
		width: 150,
		height: 150,
		borderRadius: 100,
	},
	button: {
		fontSize: 20,
		marginTop: 10,
	},
	background: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		height: 300,
	},
});
