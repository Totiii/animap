import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, View, Dimensions, Text, Image, ScrollView, Alert} from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import {Avatar, Icon} from 'react-native-elements';

const DetailAnnonce = (props) => {
	const [announce] = useState(props.route.params.markerElem)

	const [loading, setLoading] = useState(false);

	return (
		<ScrollView style={styles.container}>
			<View style={styles.view_avatar}>
				<Image
					style={styles.avatar}
					source={{
						uri: announce.avatar,
					}}
				/>
			</View>
			<View style={styles.fieldset}>
				<Text style={styles.text_title}>Description</Text>
				<Text>{announce.description}</Text>
			</View>
			<View style={styles.fieldset}>
				<Text style={styles.text_title}>Localisation</Text>
				<MapView
				showsUserLocation={true}
				style={styles.map}
				region={{
					latitude: announce.coordinate.latitude,
					longitude: announce.coordinate.longitude,
					latitudeDelta: 0.10,
					longitudeDelta: 0.10,
				}} >
					<Marker coordinate={{'latitude': announce.coordinate.latitude, 'longitude': announce.coordinate.longitude}}>
					</Marker>
				</MapView>
			</View>
			<View style={styles.last_fieldset}>
				<Text style={styles.text_title}>Contact</Text>
				<Text><Icon name='account-circle' /> {announce.user.fullname}</Text>
				<Text><Icon name='local-phone' /> {announce.user.phone_number}</Text>
				<Text><Icon name='contact-mail' /> {announce.user.mail}</Text>
			</View>

		</ScrollView>
	);
}

export default DetailAnnonce;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10
	},
	view_avatar: {
		alignItems: 'center'
	},
	avatar: {
		width: 130,
		height: 130,
		borderRadius: 100,
		marginBottom: 15,
	},
	fieldset: {
    	borderWidth: 1,
		borderRadius: 3,
		margin: 5,
		padding: 5,
		marginBottom: 5
	},
	last_fieldset: {
		borderWidth: 1,
		borderRadius: 3,
		margin: 5,
		padding: 5,
		marginBottom: 25
	},
	text_title: {
		fontSize:18,
		marginTop:10,
		marginBottom:10,
		fontWeight:'bold'
	},
	map: {
		height: 200
	}
});
