import React from 'react';
import {createContext, useEffect, useState} from 'react';
import {getAsyncStorageToken, setAsyncStorageToken} from "../api/token";
import {editUserData, getAndSetUserData, getUserData} from "../api/user";

const initialUserContext = {
    userToken: null,
    userProfile: {},
    getUserToken: () => {},
    setUserToken: () => {},
    getUserInfo: () => {},
    editUserInfo: () => {},
}

export const UserContext = createContext(initialUserContext);

export const UserProvider = ({ children }) => {
    const [userToken, setToken] = useState(null);
    const [userProfile, setUserProfile] = useState({});

    const getUserToken = () => {
        getAsyncStorageToken().then(newToken => setToken(newToken))
    }

    const getUserInfo = () => {
        getUserData().then(user => setUserProfile(user))
    }

    useEffect(() => {
        getUserToken()
        getUserInfo()
    }, [])

    const setUserToken = async (newToken) => {
        await setAsyncStorageToken(newToken)
        setToken(newToken)
    }

    const setUserInfo = async (newToken) => {
        await getAndSetUserData(newToken)
        getUserInfo()
    }

    const editUserInfo = async (newToken, firstName, lastName, phoneNumber) => {
        await editUserData(newToken, firstName, lastName, phoneNumber)
        getUserInfo()
    }

    return <UserContext.Provider value={{
        userToken,
        getUserToken,
        setUserToken,
        setUserInfo,
        userProfile,
        getUserInfo,
        editUserInfo,
    }}>
        {children}
    </UserContext.Provider>
}
