import React, {useContext, useEffect, useState} from "react";
import {View, StyleSheet, Text, TouchableOpacity, ScrollView, Alert} from "react-native";
import { Input, Button, Card, Slider  } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from "@react-native-picker/picker";
import FormData from "form-data";
import * as Location from "expo-location";
import {UserContext} from "./userContext";
import {AlertContext} from "./alertContext";

const AddAlert = ({ navigation }) => {
    const [search_range, set_search_range] = useState(1);
    const [search_type, set_search_type] = useState("dog");

    const {newAlert, setAlerts} = useContext(AlertContext)
    const {userToken} = useContext(UserContext)

    async function getLocalisation() {
        let permissionResult = await Location.requestForegroundPermissionsAsync();
        if (permissionResult.granted === false) {
            alert('Vous ne pouvez pas enregistrer un animal sans rensigner votre localisation...');
            return;
        }
        let location = await Location.getCurrentPositionAsync({});
        return [location.coords.latitude, location.coords.longitude]
    }

    async function addAlert() {

        let localisation = await getLocalisation();

        const delete_form = new FormData();
        delete_form.append('animal', search_type);
        delete_form.append('location_x', localisation[1]);
        delete_form.append('location_y', localisation[0]);
        delete_form.append('location_range', search_range);

        await newAlert({
            method: 'post',
            url: `http://6573-109-9-150-123.ngrok.io/api/search`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${userToken}`
            },
            data : delete_form
        })
        await setAlerts(userToken)
        navigation.navigate('profil_alertes')
    }

    return (
        <ScrollView style={[styles.container, {flexDirection: "column"}]}>
            <View flex={1}>
                <Text style={{ marginTop: 50, fontSize: 35, textAlign: 'center'}}>Ajouter une alerte</Text>
            </View>
            <View style={{ flex: 5}}>
                <Card>
                    <Text>Type d'animal</Text>
                    <Picker
                        selectedValue={search_type}
                        onValueChange={(value, index) => set_search_type}
                        style={styles.picker}
                    >
                        <Picker.Item label="Chien" value="dog" />
                        <Picker.Item label="Chat" value="cat" />
                        <Picker.Item label="Serpent" value="snake" />
                        <Picker.Item label="Poisson" value="fish" />
                        <Picker.Item label="Oiseau" value="bird" />
                        <Picker.Item label="Rat" value="rat" />
                    </Picker>


                    <Text style={{ marginTop:10 }} >Rayon de recherche (depuis votre localisation actuelle)</Text>
                    <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                        <Slider
                            step={1}
                            maximumValue={450}
                            minimumValue={1}
                            value={search_range}
                            thumbStyle={{ height: 20, width: 20, backgroundColor: 'blue' }}
                            onValueChange={(value) => set_search_range(value)}
                        />
                        <Text>{search_range} Km</Text>
                    </View>
                    <Button style={{ marginTop:10 }}
                            icon={
                            <Icon
                                name="search"
                                size={15}
                                color="white"
                            />
                        }
                            onPress={() => addAlert()}
                        title=" Rechercher"
                    />
                </Card>
            </View>
        </ScrollView >
    );
}

export default AddAlert;

const styles = StyleSheet.create({
    button: {
      margin: 5,
    },
    container: {
        flex: 1,
        padding: 2,
    },
})
