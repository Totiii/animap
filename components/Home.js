import React, {useState, useEffect, useContext, useRef} from 'react';
import {StyleSheet, View, Dimensions, Alert, RefreshControl} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import * as Location from 'expo-location';
import {UserContext} from "./userContext";
import {Avatar, FAB} from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import {AnnounceContext} from "./announceContext";


const Home = ({ navigation }) => {
	const [errorMsg, setErrorMsg] = useState(null);

	const {userToken, setUserToken} = useContext(UserContext)
	const [refreshing, setRefreshing] = React.useState(false);

	const [location_latitude, setLatitude] = useState(46);
	const [location_longitude, setLongitude] = useState(2);
	const [location_latitude_delta, setLatitudeDelta] = useState(20);
	const [location_longitude_delta, setLongitudeDelta] = useState(0);

	const [markers, setMarkers] = useState([]);

	const {setAnnounces, myAnnounce} = useContext(AnnounceContext)

	const {userProfile} = useContext(UserContext)

	useEffect(() => {
		(async () => {
			let { status } = await Location.requestForegroundPermissionsAsync();

			setAnnounces();

			if (status !== 'granted') {
				setErrorMsg('Permission to access location was denied');
				return;
			} else{
				try {
					let location = await Location.getCurrentPositionAsync({});
					setLatitude(location.coords.latitude);
					setLongitude(location.coords.longitude);
					setLatitudeDelta(1)
					setLongitudeDelta(1);
				} catch(e) {
					alert("Nous n'avons pas trouvé votre position. Assurez-vous d'avoir autorisé la localisation de votre appareil");
					console.log('Error while trying to get location: ', e);
				}
			}
		})();
	  }, []);



	return (
		<View style={styles.container}>
			<View>
				<MapView
					userInterfaceStyle={'dark'}
					showsUserLocation={true}
					style={styles.map}
					region={{
						latitude: location_latitude,
						longitude: location_longitude,
						latitudeDelta: location_latitude_delta,
						longitudeDelta: location_longitude_delta,
					}} >
					{
						myAnnounce.map((markerElem, i) => {
							return <Marker key={i} coordinate={markerElem.coordinate} title={markerElem.title} onPress={() => navigation.navigate('detail_annonce', { markerElem })}>
								<View>
									<Avatar
										rounded
										source={{
											uri: markerElem.avatar,
										}}
									/>
								</View>
							</Marker>
						})
					}
				</MapView>
				<FAB title="Animal trouvé"
					 onPress={userToken ? () => navigation.navigate('post') : () => navigation.navigate('Login')}
					 icon={
						 <Icon
							 name="paw"
							 size={15}
							 color="white"
						 />
				} color="blue" size="large" style={styles.btn_find_animal}>
				</FAB>
			</View>
		</View>
	);
}

export default Home;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center'
	},
	map: {
		width: Dimensions.get('window').width,
		flex: 1,
	},
	btn_find_animal: {
		borderRadius: 30,
		position: 'absolute',
		bottom: 10,
		right: 10,
	}
});
