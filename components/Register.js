import React, {useEffect, useState} from "react";
import {View, StyleSheet, Text, TouchableOpacity, ScrollView, Alert} from "react-native";
import { Input, Button, Card, Image } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
import FormData from 'form-data';
import * as Notifications from 'expo-notifications';
import Toast from "react-native-toast-message";

const Register = props => {
    const [username, onChangeUsername] = React.useState(null);
    const [email, onChangeEmail] = React.useState(null);
    const [firstName, onChangeFirstName] = React.useState(null);
    const [lastName, onChangeLastName] = React.useState(null);
    const [password, onChangePassword] = React.useState(null);
    const [phone, onChangePhone] = React.useState(null);
    const [push_token, setPushToken] = React.useState(null);
    const [creating, setCreating] = React.useState(false)

    const registerForPushNotificationsAsync = async () => {

        const { status: existingStatus } = await Notifications.getPermissionsAsync();
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            const { status } = await Notifications.requestPermissionsAsync();
            finalStatus = status;
        }
        if (finalStatus !== 'granted') {
            alert('Failed to get push token for push notification!');
            return;
        }
        return (await Notifications.getExpoPushTokenAsync()).data;
    };

    function getExpoPushToken(){
        return registerForPushNotificationsAsync()
            .then(token =>{return token})
            .catch(err => console.log(err))
    }

    async function register(){
        setCreating(true)
        const data = new FormData();
        data.append('username', username);
        data.append('email', email);
        data.append('first_name', firstName);
        data.append('last_name', lastName);
        data.append('password', password);
        data.append('phone_number', phone);
        data.append('push_token', await getExpoPushToken());


        const config = {
            method: 'post',
            url: `http://6573-109-9-150-123.ngrok.io/api/register`,
            headers: {
                'Content-Type': 'application/json'
            },
            data : data
        };

        axios(config)
            .then(function (response) {
                props.navigation.navigate('Login')
                Toast.show({
                    type: 'success',
                    text1: 'Création du compte réussis.',
                    text2: 'Vous pouvez vous connecter.'
                });
            })
            .catch(function (error) {
                Alert.alert('There was an error creating your account. Please try again.')
                console.log(error)
            }).finally(() => setCreating(false));
    }

    return (
        <ScrollView style={[styles.container, {flexDirection: "column"}]}>
            <View flex={1}>
                <Text style={{ marginTop: 50, fontSize: 40, textAlign: 'center'}}>Créer un compte</Text>
            </View>
            <View style={{ flex: 5}}>
                <Card>
                    <Input
                        label="Nom d'utilisateur"
                        placeholder="Nom d'utilisateur"
                        onChangeText={onChangeUsername}
                        value={username}
                    />
                    <Input
                        label="Nom"
                        placeholder="Nom"
                        onChangeText={onChangeLastName}
                        value={lastName}
                    />
                    <Input
                        label="Prenom"
                        placeholder="Prenom"
                        onChangeText={onChangeFirstName}
                        value={firstName}
                    />
                    <Input
                        label='Email'
                        placeholder='Email'
                        leftIcon={
                            <Icon
                                name='envelope'
                                size={24}
                                color='black'
                            />
                        }
                        value={email}
                        onChangeText={onChangeEmail}
                    />
                    <Input
                        label='Numéro de téléphone'
                        placeholder='Numéro de téléphone'
                        leftIcon={
                            <Icon
                                name='phone'
                                size={24}
                                color='black'
                            />
                        }
                        value={phone}
                        onChangeText={onChangePhone}
                    />
                    <Input
                        label='Mot de passe'
                        placeholder='Mot de passe'
                        secureTextEntry={true}
                        leftIcon={
                            <Icon
                                name='lock'
                                size={24}
                                color='black'
                            />
                        }
                        value={password}
                        onChangeText={onChangePassword}
                    />
                    <Button
                        onPress={register}
                        loading={creating}
                        icon={
                            <Icon
                                name="user-plus"
                                size={15}
                                color="white"
                            />
                        }
                        title="Créer mon compte"
                    />
                </Card>
            </View>
            <View style={{ flex: 1, marginBottom: 10}}>
                <Card>
                    <Text>Vous avez déjà un compte ? <Text onPress={() => props.navigation.navigate('Login')} style={{ color: 'blue' }}>Connectez-vous !</Text></Text>
                </Card>
            </View>
        </ScrollView >
    );
}

export default Register;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 2,
    },
})
