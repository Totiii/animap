import React from 'react';
import {createContext, useEffect, useState} from 'react';
import {
    createAnAnnounce,
    getAndSetAnnounceData,
    getAndSetUserAnnounceData,
    getAnnounceData,
    getUserAnnounceData
} from "../api/annonce";

const initialAnnounceContext = {
    myUserAnnounce: [],
    myAnnounce: [],
    getUserAnnounce: () => {},
    setUserAnnounce: () => {},
    getAnnounces: () => {},
    setAnnounces: () => {},
    newAnnounces: () => {},
}

export const AnnounceContext = createContext(initialAnnounceContext);

export const AnnounceProvider = ({ children }) => {
    const [myUserAnnounce, setMyUserAnnounce] = useState([]);
    const [myAnnounce, setMyAnnounce] = useState([]);

    const getUserAnnounce = () => {
        getUserAnnounceData().then(announce => setMyUserAnnounce(announce))
    }

    const getAnnounces = () => {
        getAnnounceData().then(announce => setMyAnnounce(announce))
    }

    useEffect(() => {
        getUserAnnounce()
        getAnnounces()
    }, [])

    const setUserAnnounce = async (token) => {
        await getAndSetUserAnnounceData(token)
        getUserAnnounce()
    }

    const setAnnounces = async () => {
        await getAndSetAnnounceData()
        getAnnounces()
    }

    const newAnnounces = async (config) => {
        await createAnAnnounce(config)
    }


    return <AnnounceContext.Provider value={{
        getUserAnnounce,
        setUserAnnounce,
        myUserAnnounce,
        myAnnounce,
        getAnnounces,
        setAnnounces,
        newAnnounces
    }}>
        {children}
    </AnnounceContext.Provider>
}
