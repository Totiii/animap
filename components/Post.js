import React, {useContext, useState} from "react";
import {View, StyleSheet, Text, ScrollView, Image, Alert} from "react-native";
import { Input, Button, Card  } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Picker } from "@react-native-picker/picker";
import * as ImagePicker from 'expo-image-picker';
import FormData from "form-data";
import axios from "axios";
import * as Location from "expo-location";
import {AnnounceContext} from "./announceContext";
import {UserContext} from "./userContext";

const Post = ({ navigation }) => {
    const [description, onChangeDescription] = React.useState(null);
    const [animal_type, set_animal_type] = useState("dog");
    const [pickedImagePath, setPickedImagePath] = useState('');
    const [loading, setLoading] = useState(false);

    const {newAnnounces, setAnnounces, setUserAnnounce} = useContext(AnnounceContext)
    const {userToken} = useContext(UserContext)

    async function getLocalisation() {
        let permissionResult = await Location.requestForegroundPermissionsAsync();
        if (permissionResult.granted === false) {
            alert('Vous ne pouvez pas enregistrer un animal sans rensigner votre localisation...');
            return;
        }
        let location = await Location.getCurrentPositionAsync({});
        return [location.coords.latitude, location.coords.longitude]
    }

    const openCamera = async () => {
        // Ask the user for the permission to access the camera
        const permissionResult = await ImagePicker.requestCameraPermissionsAsync();

        if (permissionResult.granted === false) {
            alert("Vous ne pouvez pas enregistrer un animal sans le prendre en photos...");
            return;
        }

        const result = await ImagePicker.launchCameraAsync({allowsEditing:true});

        if (!result.cancelled) {
            setPickedImagePath(result.uri);
        }
    }

    async function sendPost(){
        setLoading(true)

        let uriParts = pickedImagePath.split('.');
        let fileType = uriParts[uriParts.length - 1];
        let localisation = await getLocalisation();
        const form_post = new FormData();
        form_post.append('location_x', localisation[1])
        form_post.append('location_y', localisation[0]);
        form_post.append('animal', animal_type);
        form_post.append('description', description);
        form_post.append("image", {
            uri: pickedImagePath,
            name: `test.${fileType}`,
            type: `image/${fileType}`,
        });

        await newAnnounces({
            method: 'post',
            url: `http://6573-109-9-150-123.ngrok.io/api/post`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${userToken}`
            },
            data : form_post
        })
        await setAnnounces()
        await setUserAnnounce(userToken)
        setLoading(false)
        navigation.navigate('profil_annonces')
    }

    return (
        <ScrollView style={[styles.container, {flexDirection: "column"}]}>
            <View flex={1}>
                <Text style={{ marginTop: 50, fontSize: 35, textAlign: 'center'}}>J'ai trouvé un animal</Text>
            </View>
            <View style={{ flex: 5}}>
                <Card>
                    <Text style={styles.title}>Type d'animal</Text>
                    <Picker
                        selectedValue={animal_type}
                        onValueChange={(value, index) => set_animal_type(value)}
                        style={styles.picker}
                    >
                        <Picker.Item label="Chien" value="dog" />
                        <Picker.Item label="Chat" value="cat" />
                        <Picker.Item label="Serpent" value="snake" />
                        <Picker.Item label="Poisson" value="fish" />
                        <Picker.Item label="Oiseau" value="bird" />
                        <Picker.Item label="Rat" value="rat" />
                    </Picker>
                    <Text style={styles.title}>Description</Text>
                    <Input
                        onChangeText={onChangeDescription}
                        value={description}
                        placeholder="Description"
                    />
                    <Button
                        style={{ padding: 20}}
                        onPress={openCamera}
                        icon={
                            <Icon
                                name="camera"
                                size={15}
                                color="white"
                            />
                        }
                        title=" Prendre une photo"
                    />

                    <View style={{ padding: 30}}>
                        {
                            pickedImagePath !== '' && <Image
                                source={{ uri: pickedImagePath }}
                                style={styles.image}
                            />
                        }
                    </View>
                    {
                        pickedImagePath !== "" && description !== "" && <Button
                            style={{ padding: 20}}
                            onPress={sendPost}
                            loading={loading}
                            icon={
                                loading ? <Icon
                                        name="hourglass-half"
                                        size={15}
                                        color="white"
                                    /> :
                                <Icon
                                    name="paper-plane"
                                    size={15}
                                    color="white"
                                />
                            }
                            title={" Envoyer"}
                        />
                    }

                </Card>
            </View>
        </ScrollView >
    );
}

export default Post;

const styles = StyleSheet.create({
    button: {
        margin: 5,
    },
    container: {
        flex: 1,
        padding: 2,
    },
    imageContainer: {
        padding: 30
    },
    image: {
        height: 200,
        resizeMode: 'contain'
    },
    picker: {
        marginTop: 5,
        marginBottom: 10,
    },
    title: {
        marginBottom: 5,
        textAlign: "left",
        paddingLeft: 10
    }
})
