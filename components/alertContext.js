import React from 'react';
import {createContext, useEffect, useState} from 'react';
import {createAnAlert, deleteAnAlert, getAlertsData, getAndSetAlertsData} from "../api/alert";

const initialAlertContext = {
    myAlerts: [],
    getAlerts: () => {},
    setAlerts: () => {},
    newAlerts: () => {},
    deleteAlerts: () => {},
}

export const AlertContext = createContext(initialAlertContext);

export const AlertProvider = ({ children }) => {
    const [myAlerts, setMyAlerts] = useState([]);

    const getAlerts = () => {
        getAlertsData().then(announce => setMyAlerts(announce))
    }

    useEffect(() => {
        getAlerts()
    }, [])

    const setAlerts = async (token) => {
        await getAndSetAlertsData(token)
        getAlerts()
    }

    const newAlert = async (config) => {
        await createAnAlert(config)
    }

    const deleteAlerts = async (config) => {
        await deleteAnAlert(config)
    }

    return <AlertContext.Provider value={{
        myAlerts,
        getAlerts,
        setAlerts,
        newAlert,
        deleteAlerts
    }}>
        {children}
    </AlertContext.Provider>
}