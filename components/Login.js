import React, {useContext, useEffect, useState} from "react";
import {View, StyleSheet, Text, TouchableOpacity, Alert} from "react-native";
import { Input, Button, Card, Image } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import FormData from "form-data";
import axios from "axios";
import {UserContext} from "./userContext";
import Register from "./Register";
import Toast from "react-native-toast-message";


const Login = props => {
    const [username, onChangeUsername] = React.useState(null);
    const [password, onChangePassword] = React.useState(null);
    const [connecting, setConnecting] = React.useState(false)

    const {setUserToken, setUserInfo} = useContext(UserContext)

    async function login(){
        setConnecting(true)
        const data_get_refresh = new FormData();
        data_get_refresh.append('username', username)
        data_get_refresh.append('password', password);

        const config_get_refresh = {
            method: 'post',
            url: `http://6573-109-9-150-123.ngrok.io/api/token/`,
            headers: {
                'Content-Type': 'application/json'
            },
            data : data_get_refresh
        };

        const data_get_token = new FormData();
        const config_get_token = {
            method: 'post',
            url: `http://6573-109-9-150-123.ngrok.io/api/token/refresh/`,
            headers: {
                'Content-Type': 'application/json'
            },
            data : data_get_token
        };

        axios(config_get_refresh)
            .then(function (response) {
                data_get_token.append('refresh', response.data.refresh)
                axios(config_get_token)
                    .then(async function (response) {
                        setUserToken(response.data.access)
                        setUserInfo(response.data.access)
                        props.navigation.navigate('home')
                        Toast.show({
                            type: 'success',
                            text1: 'Vous êtes connecté :)'
                        });
                    }).finally(() => setConnecting(false));
            })
            .catch(function (error) {
                console.log(error)
                setConnecting(false)
                Alert.alert('Une erreur est survenue. Impossible de vous connecter. Veuiller reesayer ulterieurement.')
            });
    }

    return (
        <View style={[styles.container, {flexDirection: "column"}]}>
            <View flex={1}>
                <Text style={{ marginTop: 50, fontSize: 40, textAlign: 'center'}}>Connexion</Text>
            </View>
            <View style={{ flex: 4}}>
                <Card>
                    <Card.Title>Connectez-vous pour avoir accès à toutes les fonctionalitées</Card.Title>
                    <Input
                        label="Nom d'utilisateur"
                        placeholder="Nom d'utilisateur"
                        leftIcon={
                            <Icon
                                name='user'
                                size={24}
                                color='black'
                            />
                        }
                        onChangeText={onChangeUsername}
                        value={username}
                    />
                    <Input
                        label='Mot de passe'
                        secureTextEntry={true}
                        placeholder='Mot de passe'
                        leftIcon={
                            <Icon
                                name='lock'
                                size={24}
                                color='black'
                            />
                        }
                        value={password}
                        onChangeText={onChangePassword}
                    />
                    <Button
                        onPress={login}
                        loading={connecting}
                        icon={
                            <Icon
                                name="arrow-right"
                                size={15}
                                color="white"
                            />
                        }
                        title="Se connecter"
                    />
                </Card>
            </View>
            <View style={{ flex: 1 }}>
                <Card>
                    <Text>Besoin d'un compte ? <Text onPress={ () => props.navigation.navigate('register')} style={{ color: 'blue' }}>Créer un compte !</Text></Text>
                </Card>
            </View>
        </View>
    );
}

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 2,
    },
})
