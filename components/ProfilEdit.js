import React, {useState, useEffect, useContext} from 'react';
import { StyleSheet, TextInput, Text, View, TouchableOpacity } from 'react-native';
import { Input, Button, Card, Icon } from 'react-native-elements';
import {UserContext} from "./userContext";
import {LinearGradient} from "expo-linear-gradient";

const ProfilEdit = ({ navigation }) => {

	const {userProfile, userToken, editUserInfo} = useContext(UserContext)
	const [firstName, changeFirstName] = useState(userProfile['first_name']);
	const [lastName, changeLastName] = useState(userProfile['last_name']);
	const [phoneNumber, changePhoneNumber] = useState(userProfile['phone']);

	function edit() {
		editUserInfo(userToken, firstName, lastName, phoneNumber)
		navigation.navigate('profil')
	}

	return (
		<View style={styles.container}>
			<LinearGradient
				// Background Linear Gradient
				colors={['rgb(109,149,224)', 'transparent']}
				style={styles.background}
			/>
			<Card>
				<Card.Title>Mes informations</Card.Title>
				<Input
					style={styles.input}
					placeholder='Prénom'
					onChangeText={changeFirstName}
					value={firstName}
					leftIcon={
						<Icon
							name='edit'
							size={20}
							color='black'
						/>
					}
				/>
				<Input
					style={styles.input}
					placeholder='Nom'
					onChangeText={changeLastName}
					value={lastName}
					leftIcon={
						<Icon
						name='edit'
						size={20}
						color='black'
						/>
					}
				/>
				<Input
					style={styles.input}
					placeholder='Téléphone'
					onChangeText={changePhoneNumber}
					value={phoneNumber}
					leftIcon={
						<Icon
						name='edit'
						size={20}
						color='black'
						/>
					}
				/>
				<TouchableOpacity
					style={styles.button}
					onPress={edit}
				>
					<Text style={{color:'white'}}>Enregistrer</Text>
				</TouchableOpacity>
			</Card>
		</View>
	);
}

export default ProfilEdit;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#1c84a3',
		justifyContent: 'center',
	},
	button: {
		alignItems: "center",
		backgroundColor: "#3498DB",
		borderRadius: 5,
		padding: 10
	},
	background: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		height: 300,
	},
});
