import React, {useContext, useEffect, useState} from 'react';
import {RefreshControl, StyleSheet, View, ScrollView} from 'react-native';
import {Avatar, Card, Text, Button, Icon} from 'react-native-elements';
import {LinearGradient} from "expo-linear-gradient";
import {AnnounceContext} from "./announceContext";
import {UserContext} from "./userContext";

const ProfilAnnonce = ({ navigation }) => {

	const {setUserAnnounce, myUserAnnounce} = useContext(AnnounceContext)
	const {userToken} = useContext(UserContext)

	const [refreshing, setRefreshing] = React.useState(false);

	const onRefresh = React.useCallback(async () => {
		setUserAnnounce(userToken)
	}, []);

	useEffect(() => {
		setUserAnnounce(userToken)
	}, [])

	return (
		<View style={styles.container}>
			<LinearGradient
				// Background Linear Gradient
				colors={['rgb(109,149,224)', 'transparent']}
				style={styles.background}
			/>
			<ScrollView
				refreshControl={
					<RefreshControl
						refreshing={refreshing}
						onRefresh={onRefresh}
					/>
				}>
				{

					myUserAnnounce && myUserAnnounce.length > 0 ? myUserAnnounce.map((item) => (
						<Card key={item['id']}>
							<View style={{ flexDirection: "row" }}>
								<View style={{ flexDirection: "column", flexShrink: 1, flex: 4}}>
									<Text style={{ fontWeight: 'bold', fontSize: 20}}>{item['animal']}</Text>
									<Text style={{ flexShrink: 1 }}>{item['description'].substring(0,150)}{item['description'].length > 150 ? " ..." : ""}</Text>
								</View>
								<View style={{ flexDirection: "column", flexWrap: "wrap", flex: 2, justifyContent: "center", padding: 3}}>
									<Avatar
										size={100}
										source={{
											uri: "http://6573-109-9-150-123.ngrok.io" + item['image'],
										}}
									/>
								</View>
							</View>
						</Card>
					)): <Card>
						<View>
							<Text>Vous n'avez aucun signalement...</Text>
						</View>
					</Card>
				}
			</ScrollView>
		</View>
	);
}

export default ProfilAnnonce;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#1c84a3',
		paddingBottom: 20,
	},
	avatar: {
		width: 100,
		height: 100,
		borderRadius: 100,
		marginBottom: 15,
	},
	button_text: {
		fontSize: 20,
		fontWeight: 'bold',
		color: '#65a7ec',
		marginBottom: 5
	},
	background: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		height: 300,
	},
	button: {
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#db2525",
		borderRadius: 5,
		padding: 10
	},
});
