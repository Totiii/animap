import React, {useContext} from 'react';
import Home from './Home';
import Post from './Post';
import Profil from './Profil';
import ProfilEdit from './ProfilEdit';
import ProfilAlert from './ProfilAlert';
import AddAlert from './AddAlert';
import ProfilAnnonce from './ProfilAnnonce';
import Login from './Login';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {UserContext} from "./userContext";
import DetailAnnonce from './DetailAnnonce';
import Register from "./Register";

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const ProfilStack = () => {
	return (
		<Stack.Navigator initialRouteName={"profil_stack"}>
			<Stack.Screen name="profil" options={{title:'Profil'}} component={Profil} />
			<Stack.Screen name="profil_edit" options={{title:'Éditer mes informations'}} component={ProfilEdit} />
			<Stack.Screen name="profil_add_alertes" options={{title:'Nouvelle alerte'}} component={AddAlert} />
			<Stack.Screen name="profil_alertes" options={{title:'Mes alertes'}} component={ProfilAlert} />
			<Stack.Screen name="profil_annonces" options={{title:'Mes signalements'}} component={ProfilAnnonce} />
        </Stack.Navigator>
	)
}

const HomeStack = () => {
	return (
		<Stack.Navigator initialRouteName={"home_stack"} screenOptions={{
			headerStyle: {
				backgroundColor: '#1e5bf4',
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				fontWeight: 'bold',
			},
		}}>
			<Stack.Screen name="home" options={{title: 'Accueil'}} component={Home} />
			<Stack.Screen name="detail_annonce" options={{title: "Détail du signalement"}} component={DetailAnnonce} />
			<Stack.Screen name="post" options={{title:'Signalement'}} component={Post} />
			<Stack.Screen name="register" options={{title:'Inscription'}} component={Register} />
		</Stack.Navigator>
	)
}

const Tabs = () => {
	const {userToken} = useContext(UserContext)
	return (
		<Tab.Navigator screenOptions={{
			tabBarIconStyle: { display: "none" },
			tabBarLabelPosition: "beside-icon",
			headerShown: false,
			tabBarLabelStyle: {
				fontWeight: "700",
				fontSize: 15,
				marginLeft: -5
			}
		}}>
			<Tab.Screen name="home_stack" options={{
				title: 'Accueil',
				headerStyle: {
					backgroundColor: '#f4511e',
				},
				headerTintColor: '#fff',
				headerTitleStyle: {
					fontWeight: 'bold',
				},
			}} component={HomeStack} />
			{ userToken ? (<Tab.Screen name="profil_stack" options={{
				title: 'Profil',
				headerStyle: {
					backgroundColor: '#f4511e',
				},
				headerTintColor: '#fff',
				headerTitleStyle: {
					fontWeight: 'bold',
				},
			}} component={ProfilStack} />) : (<Tab.Screen name="Login" component={Login} />) }
		</Tab.Navigator>
	);
}

export default Tabs;
