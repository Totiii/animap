import {Alert, AsyncStorage} from "react-native";
import axios from "axios";
import Toast from "react-native-toast-message";
import FormData from "form-data";

export const getAndSetAlertsData = async (token) => {

    await axios({
        method: 'get',
        url: `http://6573-109-9-150-123.ngrok.io/api/search`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    })
        .then(async function (response) {
            await AsyncStorage.setItem('alerts', JSON.stringify(response.data['searchs']));
        })
        .catch(function (error) {
            console.log(error)
            Alert.alert('There was an error. Please try again.')
        });
};

export const createAnAlert = async (config) => {

    await axios(config)
        .then(() => {
            console.log('success')
        })
        .catch(function (error) {
            console.log(error)
        });
}

export const deleteAnAlert = async (config) => {

    await axios(config)
        .then(() => {
            console.log('success')
        })
        .catch(function (error) {
            console.log(error)
        });
}

export const getAlertsData = async () => {
    try {
        const value = await AsyncStorage.getItem('alerts');
        if (value !== null) {
            return JSON.parse(value);
        }
    } catch (e) {
        return null;
    }
};