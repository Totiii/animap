import axios from "axios";
import {AsyncStorage} from "react-native";
import FormData from "form-data";

export const getAndSetUserData = async (token) => {

    let user = {}
    const config = {
        method: 'get',
        url: `http://6573-109-9-150-123.ngrok.io/api/user`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    };

    await axios(config)
        .then(async function (response) {
            user = response.data
            user['avatar'] = "http://6573-109-9-150-123.ngrok.io" + response.data['avatar']
            await AsyncStorage.setItem('user', JSON.stringify(user));
        })
        .catch(async function (error) {
            console.log(error)
            await AsyncStorage.setItem('user', "");
        });
};

export const getUserData = async () => {
    try {
        const value = await AsyncStorage.getItem('user');
        if (value !== null) {
            return JSON.parse(value);
        }
    } catch (e) {
        return null;
    }
};


export const editUserData = async (token, firstName, lastName, phoneNumber) => {

    var data = new FormData();
    data.append('first_name', firstName);
    data.append('last_name', lastName);
    data.append('phone', phoneNumber);

    let user = {}
    const config = {
        method: 'patch',
        url: `http://6573-109-9-150-123.ngrok.io/api/user`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        data: data
    };

    await axios(config)
        .then(async function (response) {
            user = response.data
            user['avatar'] = "http://6573-109-9-150-123.ngrok.io" + response.data['avatar']
            await AsyncStorage.setItem('user', JSON.stringify(user));
        })
        .catch(async function (error) {
            console.log(error)
        });
};
