import { AsyncStorage } from 'react-native';

export const getAsyncStorageToken = async () => {
    try {
        const value = await AsyncStorage.getItem('@auth_token');
        if (value !== null) {
            return value;
        }
    } catch (e) {
        return null;
    }
};

export const setAsyncStorageToken = async (token) => {
    try {
        await AsyncStorage.setItem('@auth_token', token);
    } catch (e) {
        return null;
    }
};
