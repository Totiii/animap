import axios from "axios";
import {Alert, AsyncStorage} from "react-native";
import Toast from "react-native-toast-message";

export const getAndSetAnnounceData = async (token) => {

    let markers = []

    const config = {
        method: 'get',
        url: `http://6573-109-9-150-123.ngrok.io/api/post`,
        headers: {
            'Content-Type': 'application/json'
        },
    };

    await axios(config)
        .then(async function (response) {
            if (response.data['nb_posts'] > 0) {
                await response.data['posts'].forEach(post => {
                    markers.push({
                        coordinate: {
                            "latitude": post['location'][1],
                            "longitude": post['location'][0],
                        },
                        avatar: "http://6573-109-9-150-123.ngrok.io" + post['image'],
                        api_url: post['api_url'],
                        description: post['description'],
                        user: post['user'],
                    })
                })
            }
            await AsyncStorage.setItem('announce', JSON.stringify(markers));
        })
        .catch(async function (error) {
            console.log(error)
            Alert.alert('Erreur lors de la récupération des signalements. Vérifier votre connexion internet.')
        });
};

export const getAnnounceData = async () => {
    try {
        const value = await AsyncStorage.getItem('announce');
        if (value !== null) {
            return JSON.parse(value);
        }
    } catch (e) {
        return null;
    }
};

export const getAndSetUserAnnounceData = async (token) => {

    const config = {
        method: 'get',
        url: `http://6573-109-9-150-123.ngrok.io/api/post`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    };

    await axios(config)
        .then(async function (response) {
            await AsyncStorage.setItem('userAnnounce', JSON.stringify(response.data['posts']));
        })
        .catch(async function (error) {
            console.log(error)
            Alert.alert('Erreur lors de la récupération des signalements. Vérifier votre connexion internet.')
            await AsyncStorage.setItem('userAnnounce', JSON.stringify([]));
        });
};

export const getUserAnnounceData = async () => {
    try {
        const value = await AsyncStorage.getItem('userAnnounce');
        if (value !== null) {
            return JSON.parse(value);
        }
    } catch (e) {
        return null;
    }
};


export const createAnAnnounce = async (config) => {

    await axios(config)
        .then(() => {
            console.log('success')
            Toast.show({
                type: 'success',
                text1: 'Votre signalement a bien été enregistré.',
                text2: 'Merci pour votre attention :)'
            });
        })
        .catch(function (error) {
            console.log(error)
        });
};
