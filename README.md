# Node version

`16.11.1`

# Install dependencies 

`npm install or yarn install`

# Create an .env file

`BASE_URL = your_backend_url`

# Start Project 

`Expo start`

