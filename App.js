import React, {useEffect, useRef } from 'react';
import { StyleSheet, Text, View , StatusBar, Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Tabs from './components/Tabs';
import Toast from 'react-native-toast-message';
import {UserProvider} from './components/userContext';
import {AnnounceProvider} from "./components/announceContext";
import {AlertProvider} from "./components/alertContext";

const App = () => {

	return (
		<UserProvider>
			<AnnounceProvider>
				<AlertProvider>
					<NavigationContainer>
						<StatusBar
							hidden = {true}
							translucent = {true}
						/>
						<Tabs />
					</NavigationContainer>
					<Toast />
				</AlertProvider>
			</AnnounceProvider>
		</UserProvider>
	);
}

export default App;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 100,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
